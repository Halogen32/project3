#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <iostream>
using namespace std;

// authors: Mitchell Myers, Maria Moore
// group: 32
// date: 3/11/2019
// modified: 3/14/2019
// purpose: complete the assignment described in Part3 of Project3 in CIS 450

sem_t person_talking; // mutex for talking
sem_t answering; // resource for answer
sem_t question; // resource for question
sem_t waiting; // contains 0 resources to force waiting
sem_t leaving;
sem_t wait_for_students;

pthread_t studentTID; // thread ID to use for student threads
pthread_t professorTID; // thread ID to use for professor thread	

void wait(unsigned int cycles) {
	for (int i = 0; i < cycles; i++);
}

class Professor {
private:

	int expectedNumStudents;

	void AnswerStart() {
		
		cout << "Professor wants to be asked a question." << endl;
		sem_post(&answering);
		
		sem_wait(&question); // wait for student to ask a question
		sem_wait(&person_talking); // wait until the student is done asking their question
		
		sched_yield();
		
	}
	
	void AnswerDone() {
	
		cout << "Professor is finished answering the question." << endl;
		
		sem_post(&person_talking);
		sem_post(&waiting); // tell the student to leave because their question was answered
		sem_wait(&leaving); // wait for the student to leave

		// sem_post(&answering); // allow another student to ask a question
		// sem_post(&person_talking); // allow another student to talk
		
		sched_yield();
		
	}

public:

	Professor(int x) {
		
		expectedNumStudents = x;
		
	}
	
	void Answer() {
	
		// sem_wait(&wait_for_students);
		while (expectedNumStudents > 0) {
			
			AnswerStart();
			cout << "The professor is answering the question." << endl;
			AnswerDone();
			
			expectedNumStudents--;
		}
		
	}

};

class Student {
private:

	static unsigned int nextID;
	unsigned int ID;

	void QuestionStart() {		
		
		

		// cout << "Student " << ID << " is ready to ask a question." << endl;
		
		
		printf("Student %d : The student is ready to ask a question\n", ID);
		// sem_post(&wait_for_students);
		
		sem_wait(&answering); // wait for turn to ask question		
		sem_wait(&person_talking); // wait for teacher to stop talking

		sched_yield();
		
	}
	
	void QuestionDone() {
	
		sem_post(&person_talking); // allow teacher to talk
		sem_post(&question); // allow teacher to answer the question
		
		//cout << "Student " << ID << " is done asking a question and waits for an answer." << endl;
		printf("Student %d : The student is done asking a question.\n", ID);
		
		sem_wait(&waiting); // wait for the teacher to answer the question
		//cout << "Student " << ID << " received an answer and left." << endl;
		printf("Student %d : The student receives an answer and leaves.\n", ID);

		sem_post(&leaving); // student has left

		sched_yield();
		
	}
	
public:

	Student() {
		
		ID = nextID;
		nextID++;
		
	}
	
	void Ask() {
		
		// wait(rand()); // force any thread that reaches this point to wait for a random time, to force interweaving
		QuestionStart();
		// cout << "Student " << ID << " is asking a question." << endl;
		
		printf("Student %d : The student is asking their question.\n", ID);
		QuestionDone();
		
	}
};
unsigned int Student::nextID = 1; // init starting id number for student class

void * Ask(void * student) { // intermediary method to convert void* to class and access its entry method
	Student *s = (Student *) student;
	
	s->Ask();
}

void * Answer(void * professor) { // intermediary method to convert void* to class and access its entry method
	Professor *p = (Professor *) professor;
	
	p->Answer();
}


int main(int argc, char* argv[]) {
	const int SHARED = 1; // semaphores are shared across threads
	const int EXPECTED_ARGC = 2; // number of expected arguments passed through the cmd line
	int numStudents; // # of students that want to ask the teacher a question
	
	// end the program if not enough arguments are supplied
	if (argc < EXPECTED_ARGC) {
		printf("usage: profstud.out <num students>\n");
		exit(0);
	}
	
	srand(time(0));
	numStudents = atoi(argv[1]); // convert string number to integer
	
	Student * students = new Student[numStudents];
	Professor * professor = new Professor(numStudents);
	
	// initialize semaphore variables
	sem_init(&person_talking, SHARED, 1);
	sem_init(&answering, SHARED, 0);
	sem_init(&question, SHARED, 0);
	sem_init(&waiting, SHARED, 0);
	sem_init(&leaving, SHARED, 0);
	sem_init(&wait_for_students, SHARED, (numStudents - 1) * -1);
	
	// start multithreading
	for (int i = 0; i < numStudents; i++) {
		pthread_create(&studentTID, NULL, Ask, (void *) &students[i]); // assign all student threads to their asking block
	}
	pthread_create(&professorTID, NULL, Answer, (void *) professor); // assign professor thread to their answer block
	
	// end multithreading
	pthread_join(professorTID, NULL); // block all threads until the last student thread to reach this line
	pthread_exit(0); // end the program
}







