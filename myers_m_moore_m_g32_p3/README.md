#GENERAL#
Run 'make all' from the cmd line to generate .exe files from source for each application. Commands with a '<text>' indicate that 
the program requires a parameter that the user is to provide in the command line as part of the execution. If you are using the
Windows cmd line, the command to execute the program is simply the <name of the program>.exe; otherwise, you must precede the command
with "./" to execute it.

Alternatively, you can run 'make <application name>' to just create a particular application and nothing else.
To remove all applications from the directory they are created in, run the 'make clean' command.

#FOREWORD#
Because of the nature of multithreading environments, it's difficult to debug if threads are working as intended.
To verify correct functionality, random wait times were calculated and applied to individual threads to wait such that interleaving was enforced.
Because the random wait is a debugging tool, these are disabled but viewable in the source code.

###BOUNDED BUFFER###
Navigate to the containig folder and type 'boundedBuffer.exe <thread iterations> <buffer size>' to execute the program.

####Algorithm####
Three producer and three consumer threads (six threads total) share a buffer of size 'n' and interleave over that buffer for 'x' 
iterations. The buffer is only allowed to be manipulated by one thread at a time; so since the buffer is initially empty, a 
producer thread is the first thread to manipulate the buffer. Producers enter data into a cell and increment a local range value 
to allow the next producer thread to insert their values into the next cell, repeating until they run out of iterations. Consumer 
threads wait until there is something in the buffer to consume and total whatever is in a cell they check at the time; they follow 
the same behavior of producers in terms of thread interleaving.

The idea here is that the scheduling of threads could produce different results on subsequent runs because the value in a particular
buffer cell could change before a consumer thread can reach it, affecting the result output to the standard output stream.

####Example Output####

Output when Iterations = 100, Buffer Size = 100

$ ./boundedBuffer.exe 100 100
the total is 4475
the total is 4501
the total is 5874

$ ./boundedBuffer.exe 100 100
the total is 3899
the total is 5560
the total is 5391

Note that when the same command is run, you may get different results. This is normal and is due to the nature of how the schedular
in the operating system is deciding which thread to use.


Output when Iterations = 10000, Buffer Size = 10

$ ./boundedBuffer.exe 10000 10
the total is 49506285
the total is 50060975
the total is 50417740

$ ./boundedBuffer.exe 10000 10
the total is 49582836
the total is 50237752
the total is 50164412


Output when Iterations = 100, Buffer Size = 10

$ ./boundedBuffer.exe 100 10
the total is 2021
the total is 5115
the total is 7714

$ ./boundedBuffer.exe 100 10
the total is 2145
the total is 5925
the total is 6780

###PROFSTUD###
Navigate to the containing folder and type 'profstud.exe <numStudents>' from the cmd line to execute the program.
 
####Algorithm####
Create an arbitrary number of student instances and a single professor instance. Create threads equal to the number of those 
instances combined and bind each thread to an instance of a class into an intermediary method. Threads only execute in the 
instance of the class they are bound to, so their IDs are printed from the class they are in; the Student class contains a local 
ID instance generated when the class is instantiated. 

Student threads queue and wait for the professor thread to release resources through a semaphore system of mutex locks. The 
professor will tell the students that it is ready to answer a question, to which the first student in the semaphore queue will ask 
while the rest wait their turn. When the student is done asking their question, the professor then answers the question and lets 
the student leave. This behavior repeats until all student questions are answered, to which the professor's thread will return to
the main function, join with all the student threads, and exit the program.

####Example Output####

$ ./profstud.exe 5
Student 1 : The student is ready to ask a question
Student 2 : The student is ready to ask a question
Student 3 : The student is ready to ask a question
Student 4 : The student is ready to ask a question
Student 5 : The student is ready to ask a question
Professor wants to be asked a question.
Student 1 : The student is asking their question.
Student 1 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 1 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 2 : The student is asking their question.
Student 2 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 2 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 3 : The student is asking their question.
Student 3 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 3 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 4 : The student is asking their question.
Student 4 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 4 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 5 : The student is asking their question.
Student 5 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.

$ ./profstud.exe 15
Student 1 : The student is ready to ask a question
Student 2 : The student is ready to ask a question
Student 3 : The student is ready to ask a question
Student 4 : The student is ready to ask a question
Student 5 : The student is ready to ask a question
Student 6 : The student is ready to ask a question
Student 7 : The student is ready to ask a question
Student 8 : The student is ready to ask a question
Student 9 : The student is ready to ask a question
Student 10 : The student is ready to ask a question
Student 11 : The student is ready to ask a question
Student 12 : The student is ready to ask a question
Student 13 : The student is ready to ask a question
Student 14 : The student is ready to ask a question
Student 15 : The student is ready to ask a question
Professor wants to be asked a question.
Student 1 : The student is asking their question.
Student 1 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 1 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 2 : The student is asking their question.
Student 2 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 2 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 3 : The student is asking their question.
Student 3 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 3 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 4 : The student is asking their question.
Student 4 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 4 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 5 : The student is asking their question.
Student 5 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 5 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 6 : The student is asking their question.
Student 6 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 6 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 7 : The student is asking their question.
Student 7 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 7 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 8 : The student is asking their question.
Student 8 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 8 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 9 : The student is asking their question.
Student 9 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 9 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 10 : The student is asking their question.
Student 10 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 10 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 11 : The student is asking their question.
Student 11 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 11 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 12 : The student is asking their question.
Student 12 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 12 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 13 : The student is asking their question.
Student 13 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 13 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 14 : The student is asking their question.
Student 14 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.
Student 14 : The student receives an answer and leaves.
Professor wants to be asked a question.
Student 15 : The student is asking their question.
Student 15 : The student is done asking a question.
The professor is answering the question.
Professor is finished answering the question.

###BRIDGE###
Navigate to the containing folder and type 'bridge.exe' to execute the program.

####Algorithm####
Fifty car threads want to cross a singleton bridge instance, all of which are instantiated in the main function.
The first thread that enters the bridge will lock the bridge in a particular flow direction, 
preventing threads that want to change the direction from advancing until the bridge capacity has 0 
threads on it. The maximum amount of threads that can be at the bridge at any given time is 3. If the conditions
for crossing the bridge are not met by the thread that wants to cross, the thread is put on a waiting queue until
a car crosses the bridge. A car that crosses the bridge signals the queue to release all cars waiting to see if
any waiting cars can cross the bridge; cars that cannot are put back on the waiting queue.

During this process, the application sends messages to the standard output stream verifying which
cars are currently crossing the bridge and which ones are waiting to cross.

####Example Output####
Note that the speed at which a car crosses a bridge is dependent upon the scheduling of the system.
Interleaving was enforced using random wait times to verify functionality, but this has been disabled.
The following is output is the raw running threads with no waiting.

$ ./bridge.exe
Car 1 dir 0 arrives at bridge.
Car 1 crossing bridge. Current dir: 0 #cars: 1
Car 1 dir 0 exits from bridge.
Car 2 dir 0 arrives at bridge.
Car 2 crossing bridge. Current dir: 0 #cars: 1
Car 2 dir 0 exits from bridge.
Car 3 dir 0 arrives at bridge.
Car 3 crossing bridge. Current dir: 0 #cars: 1
Car 3 dir 0 exits from bridge.
Car 4 dir 1 arrives at bridge.
Car 4 crossing bridge. Current dir: 1 #cars: 1
Car 4 dir 1 exits from bridge.
Car 5 dir 1 arrives at bridge.
Car 5 crossing bridge. Current dir: 1 #cars: 1
Car 5 dir 1 exits from bridge.
Car 6 dir 0 arrives at bridge.
Car 6 crossing bridge. Current dir: 0 #cars: 1
Car 6 dir 0 exits from bridge.
Car 7 dir 0 arrives at bridge.
Car 7 crossing bridge. Current dir: 0 #cars: 1
Car 7 dir 0 exits from bridge.
Car 8 dir 1 arrives at bridge.
Car 8 crossing bridge. Current dir: 1 #cars: 1
Car 8 dir 1 exits from bridge.
Car 9 dir 0 arrives at bridge.
Car 9 crossing bridge. Current dir: 0 #cars: 1
Car 9 dir 0 exits from bridge.
Car 10 dir 1 arrives at bridge.
Car 10 crossing bridge. Current dir: 1 #cars: 1
Car 10 dir 1 exits from bridge.
Car 11 dir 0 arrives at bridge.
Car 11 crossing bridge. Current dir: 0 #cars: 1
Car 11 dir 0 exits from bridge.
Car 12 dir 1 arrives at bridge.
Car 12 crossing bridge. Current dir: 1 #cars: 1
Car 12 dir 1 exits from bridge.
Car 13 dir 0 arrives at bridge.
Car 13 crossing bridge. Current dir: 0 #cars: 1
Car 13 dir 0 exits from bridge.
Car 14 dir 1 arrives at bridge.
Car 14 crossing bridge. Current dir: 1 #cars: 1
Car 14 dir 1 exits from bridge.
Car 15 dir 0 arrives at bridge.
Car 15 crossing bridge. Current dir: 0 #cars: 1
Car 15 dir 0 exits from bridge.
Car 16 dir 1 arrives at bridge.
Car 16 crossing bridge. Current dir: 1 #cars: 1
Car 16 dir 1 exits from bridge.
Car 17 dir 0 arrives at bridge.
Car 17 crossing bridge. Current dir: 0 #cars: 1
Car 17 dir 0 exits from bridge.
Car 18 dir 1 arrives at bridge.
Car 18 crossing bridge. Current dir: 1 #cars: 1
Car 18 dir 1 exits from bridge.
Car 19 dir 1 arrives at bridge.
Car 19 crossing bridge. Current dir: 1 #cars: 1
Car 19 dir 1 exits from bridge.
Car 20 dir 0 arrives at bridge.
Car 20 crossing bridge. Current dir: 0 #cars: 1
Car 20 dir 0 exits from bridge.
Car 21 dir 0 arrives at bridge.
Car 21 crossing bridge. Current dir: 0 #cars: 1
Car 21 dir 0 exits from bridge.
Car 22 dir 1 arrives at bridge.
Car 22 crossing bridge. Current dir: 1 #cars: 1
Car 22 dir 1 exits from bridge.
Car 23 dir 0 arrives at bridge.
Car 23 crossing bridge. Current dir: 0 #cars: 1
Car 23 dir 0 exits from bridge.
Car 24 dir 0 arrives at bridge.
Car 24 crossing bridge. Current dir: 0 #cars: 1
Car 24 dir 0 exits from bridge.
Car 25 dir 0 arrives at bridge.
Car 25 crossing bridge. Current dir: 0 #cars: 1
Car 25 dir 0 exits from bridge.
Car 26 dir 0 arrives at bridge.
Car 26 crossing bridge. Current dir: 0 #cars: 1
Car 26 dir 0 exits from bridge.
Car 27 dir 0 arrives at bridge.
Car 27 crossing bridge. Current dir: 0 #cars: 1
Car 27 dir 0 exits from bridge.
Car 28 dir 0 arrives at bridge.
Car 28 crossing bridge. Current dir: 0 #cars: 1
Car 28 dir 0 exits from bridge.
Car 29 dir 0 arrives at bridge.
Car 29 crossing bridge. Current dir: 0 #cars: 1
Car 29 dir 0 exits from bridge.
Car 30 dir 0 arrives at bridge.
Car 30 crossing bridge. Current dir: 0 #cars: 1
Car 30 dir 0 exits from bridge.
Car 31 dir 1 arrives at bridge.
Car 31 crossing bridge. Current dir: 1 #cars: 1
Car 31 dir 1 exits from bridge.
Car 32 dir 1 arrives at bridge.
Car 32 crossing bridge. Current dir: 1 #cars: 1
Car 32 dir 1 exits from bridge.
Car 33 dir 1 arrives at bridge.
Car 33 crossing bridge. Current dir: 1 #cars: 1
Car 33 dir 1 exits from bridge.
Car 34 dir 1 arrives at bridge.
Car 34 crossing bridge. Current dir: 1 #cars: 1
Car 34 dir 1 exits from bridge.
Car 35 dir 1 arrives at bridge.
Car 35 crossing bridge. Current dir: 1 #cars: 1
Car 35 dir 1 exits from bridge.
Car 36 dir 1 arrives at bridge.
Car 36 crossing bridge. Current dir: 1 #cars: 1
Car 36 dir 1 exits from bridge.
Car 37 dir 1 arrives at bridge.
Car 37 crossing bridge. Current dir: 1 #cars: 1
Car 37 dir 1 exits from bridge.
Car 38 dir 1 arrives at bridge.
Car 38 crossing bridge. Current dir: 1 #cars: 1
Car 38 dir 1 exits from bridge.
Car 39 dir 1 arrives at bridge.
Car 39 crossing bridge. Current dir: 1 #cars: 1
Car 39 dir 1 exits from bridge.
Car 40 dir 0 arrives at bridge.
Car 40 crossing bridge. Current dir: 0 #cars: 1
Car 40 dir 0 exits from bridge.
Car 41 dir 0 arrives at bridge.
Car 41 crossing bridge. Current dir: 0 #cars: 1
Car 41 dir 0 exits from bridge.
Car 42 dir 0 arrives at bridge.
Car 42 crossing bridge. Current dir: 0 #cars: 1
Car 42 dir 0 exits from bridge.
Car 43 dir 0 arrives at bridge.
Car 43 crossing bridge. Current dir: 0 #cars: 1
Car 43 dir 0 exits from bridge.
Car 44 dir 0 arrives at bridge.
Car 44 crossing bridge. Current dir: 0 #cars: 1
Car 44 dir 0 exits from bridge.
Car 45 dir 1 arrives at bridge.
Car 45 crossing bridge. Current dir: 1 #cars: 1
Car 45 dir 1 exits from bridge.
Car 46 dir 1 arrives at bridge.
Car 46 crossing bridge. Current dir: 1 #cars: 1
Car 46 dir 1 exits from bridge.
Car 47 dir 0 arrives at bridge.
Car 47 crossing bridge. Current dir: 0 #cars: 1
Car 47 dir 0 exits from bridge.
Car 48 dir 1 arrives at bridge.
Car 48 crossing bridge. Current dir: 1 #cars: 1
Car 48 dir 1 exits from bridge.
Car 49 dir 1 arrives at bridge.
Car 49 crossing bridge. Current dir: 1 #cars: 1
Car 49 dir 1 exits from bridge.
Car 50 dir 0 arrives at bridge.
Car 50 crossing bridge. Current dir: 0 #cars: 1
Car 50 dir 0 exits from bridge.

2nd run, showing random directions.
$ ./bridge.exe
Car 1 dir 1 arrives at bridge.
Car 1 crossing bridge. Current dir: 1 #cars: 1
Car 1 dir 1 exits from bridge.
Car 2 dir 0 arrives at bridge.
Car 2 crossing bridge. Current dir: 0 #cars: 1
Car 2 dir 0 exits from bridge.
Car 3 dir 0 arrives at bridge.
Car 3 crossing bridge. Current dir: 0 #cars: 1
Car 3 dir 0 exits from bridge.
Car 4 dir 0 arrives at bridge.
Car 4 crossing bridge. Current dir: 0 #cars: 1
Car 4 dir 0 exits from bridge.
Car 5 dir 0 arrives at bridge.
Car 5 crossing bridge. Current dir: 0 #cars: 1
Car 5 dir 0 exits from bridge.
Car 6 dir 1 arrives at bridge.
Car 6 crossing bridge. Current dir: 1 #cars: 1
Car 6 dir 1 exits from bridge.
Car 7 dir 1 arrives at bridge.
Car 7 crossing bridge. Current dir: 1 #cars: 1
Car 7 dir 1 exits from bridge.
Car 8 dir 1 arrives at bridge.
Car 8 crossing bridge. Current dir: 1 #cars: 1
Car 8 dir 1 exits from bridge.
Car 9 dir 1 arrives at bridge.
Car 9 crossing bridge. Current dir: 1 #cars: 1
Car 9 dir 1 exits from bridge.
Car 10 dir 0 arrives at bridge.
Car 10 crossing bridge. Current dir: 0 #cars: 1
Car 10 dir 0 exits from bridge.
Car 11 dir 1 arrives at bridge.
Car 11 crossing bridge. Current dir: 1 #cars: 1
Car 11 dir 1 exits from bridge.
Car 12 dir 1 arrives at bridge.
Car 12 crossing bridge. Current dir: 1 #cars: 1
Car 12 dir 1 exits from bridge.
Car 13 dir 1 arrives at bridge.
Car 13 crossing bridge. Current dir: 1 #cars: 1
Car 13 dir 1 exits from bridge.
Car 14 dir 1 arrives at bridge.
Car 14 crossing bridge. Current dir: 1 #cars: 1
Car 14 dir 1 exits from bridge.
Car 15 dir 0 arrives at bridge.
Car 15 crossing bridge. Current dir: 0 #cars: 1
Car 15 dir 0 exits from bridge.
Car 16 dir 1 arrives at bridge.
Car 16 crossing bridge. Current dir: 1 #cars: 1
Car 16 dir 1 exits from bridge.
Car 17 dir 0 arrives at bridge.
Car 17 crossing bridge. Current dir: 0 #cars: 1
Car 17 dir 0 exits from bridge.
Car 18 dir 0 arrives at bridge.
Car 18 crossing bridge. Current dir: 0 #cars: 1
Car 18 dir 0 exits from bridge.
Car 19 dir 1 arrives at bridge.
Car 19 crossing bridge. Current dir: 1 #cars: 1
Car 19 dir 1 exits from bridge.
Car 20 dir 0 arrives at bridge.
Car 20 crossing bridge. Current dir: 0 #cars: 1
Car 20 dir 0 exits from bridge.
Car 21 dir 0 arrives at bridge.
Car 21 crossing bridge. Current dir: 0 #cars: 1
Car 21 dir 0 exits from bridge.
Car 22 dir 1 arrives at bridge.
Car 22 crossing bridge. Current dir: 1 #cars: 1
Car 22 dir 1 exits from bridge.
Car 23 dir 1 arrives at bridge.
Car 23 crossing bridge. Current dir: 1 #cars: 1
Car 23 dir 1 exits from bridge.
Car 24 dir 0 arrives at bridge.
Car 24 crossing bridge. Current dir: 0 #cars: 1
Car 24 dir 0 exits from bridge.
Car 25 dir 0 arrives at bridge.
Car 25 crossing bridge. Current dir: 0 #cars: 1
Car 25 dir 0 exits from bridge.
Car 26 dir 1 arrives at bridge.
Car 26 crossing bridge. Current dir: 1 #cars: 1
Car 26 dir 1 exits from bridge.
Car 27 dir 0 arrives at bridge.
Car 27 crossing bridge. Current dir: 0 #cars: 1
Car 27 dir 0 exits from bridge.
Car 28 dir 1 arrives at bridge.
Car 28 crossing bridge. Current dir: 1 #cars: 1
Car 28 dir 1 exits from bridge.
Car 29 dir 1 arrives at bridge.
Car 29 crossing bridge. Current dir: 1 #cars: 1
Car 29 dir 1 exits from bridge.
Car 30 dir 0 arrives at bridge.
Car 30 crossing bridge. Current dir: 0 #cars: 1
Car 30 dir 0 exits from bridge.
Car 31 dir 1 arrives at bridge.
Car 31 crossing bridge. Current dir: 1 #cars: 1
Car 31 dir 1 exits from bridge.
Car 32 dir 1 arrives at bridge.
Car 32 crossing bridge. Current dir: 1 #cars: 1
Car 32 dir 1 exits from bridge.
Car 33 dir 0 arrives at bridge.
Car 33 crossing bridge. Current dir: 0 #cars: 1
Car 33 dir 0 exits from bridge.
Car 34 dir 1 arrives at bridge.
Car 34 crossing bridge. Current dir: 1 #cars: 1
Car 34 dir 1 exits from bridge.
Car 35 dir 0 arrives at bridge.
Car 35 crossing bridge. Current dir: 0 #cars: 1
Car 35 dir 0 exits from bridge.
Car 36 dir 0 arrives at bridge.
Car 36 crossing bridge. Current dir: 0 #cars: 1
Car 36 dir 0 exits from bridge.
Car 37 dir 1 arrives at bridge.
Car 37 crossing bridge. Current dir: 1 #cars: 1
Car 37 dir 1 exits from bridge.
Car 38 dir 1 arrives at bridge.
Car 38 crossing bridge. Current dir: 1 #cars: 1
Car 38 dir 1 exits from bridge.
Car 39 dir 1 arrives at bridge.
Car 39 crossing bridge. Current dir: 1 #cars: 1
Car 39 dir 1 exits from bridge.
Car 40 dir 1 arrives at bridge.
Car 40 crossing bridge. Current dir: 1 #cars: 1
Car 40 dir 1 exits from bridge.
Car 41 dir 1 arrives at bridge.
Car 41 crossing bridge. Current dir: 1 #cars: 1
Car 41 dir 1 exits from bridge.
Car 42 dir 1 arrives at bridge.
Car 42 crossing bridge. Current dir: 1 #cars: 1
Car 42 dir 1 exits from bridge.
Car 43 dir 1 arrives at bridge.
Car 43 crossing bridge. Current dir: 1 #cars: 1
Car 43 dir 1 exits from bridge.
Car 44 dir 0 arrives at bridge.
Car 44 crossing bridge. Current dir: 0 #cars: 1
Car 44 dir 0 exits from bridge.
Car 45 dir 1 arrives at bridge.
Car 45 crossing bridge. Current dir: 1 #cars: 1
Car 45 dir 1 exits from bridge.
Car 46 dir 1 arrives at bridge.
Car 46 crossing bridge. Current dir: 1 #cars: 1
Car 46 dir 1 exits from bridge.
Car 47 dir 1 arrives at bridge.
Car 47 crossing bridge. Current dir: 1 #cars: 1
Car 47 dir 1 exits from bridge.
Car 48 dir 1 arrives at bridge.
Car 48 crossing bridge. Current dir: 1 #cars: 1
Car 48 dir 1 exits from bridge.
Car 49 dir 0 arrives at bridge.
Car 49 crossing bridge. Current dir: 0 #cars: 1
Car 49 dir 0 exits from bridge.
Car 50 dir 0 arrives at bridge.
Car 50 crossing bridge. Current dir: 0 #cars: 1
Car 50 dir 0 exits from bridge.





