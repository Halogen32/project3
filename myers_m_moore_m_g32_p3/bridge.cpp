#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
using namespace std;

// authors: Mitchell Myers, Maria Moore
// group: 32
// date: 3/11/2019
// modified: 3/14/2019
// purpose: satisfy the reqs defined in part 2 of project 3 for cis 350


pthread_mutex_t Monitor_lock;
pthread_cond_t NorthSouth[2]; //lock North-South cars till safe
pthread_cond_t waiting;

static int Waiting[2]; //holds # of cars waiting ~ 0 = North 1 = South

void wait(int cycles) {
	for (int i = 0; i < cycles; i++);
}
const int WAIT_TIME = 0; // cycles

struct thread_data { // data that needs to be passed for the program to work correctly
	int direction; // used to indicate what direction the car of a thread is going
	int id; // used to output a readable number indicating what thread/car is crossing the bridge
};

class Bridge { // monitor class
private:

    static int currentDirection; // indicates the direction the bridge is allowing cars to flow
    static int numCars; // indicates how many vehicles are currently occupying the bridge
	static const int MAX_CAPACITY; 
	static Bridge *instance; // singleton
	

public:

	static Bridge* GetInstance() {
		
		if (instance == nullptr) {
			instance = new Bridge();
		}
		
		return instance;
		
	}

    void ArriveBridge(thread_data td) {
		
        pthread_mutex_lock(&Monitor_lock); // block other threads until the monitor is unlocked
        cout << "Car " << td.id << " dir " << td.direction << " arrives at bridge." << endl;
		
        /* if(!isSafe(td)){
			
            //it is not safe - Wait
        	//cout << "Car " << pthread_self() << " is waiting " << endl;
			
            Waiting[td]++; //Add Car waiting in direction
			
            while (!isSafe(td) ){//wait till safe
                pthread_cond_wait(&NorthSouth[td], &Monitor_lock);
            }
			
            Waiting[td]--;//Remove Car waiting in direction
        } */
		
		while (!isSafe(td.direction)) {
			
			/*
			
				if it's safe to cross the bridge, the thread will exit the loop and cross;
				else, the thread will cycle through the loop again transfer monitor usage to the next thread;
			
			*/
			
			cout << "Car " << td.id << " is waiting " << endl;
			//pthread_cond_wait(&NorthSouth[td], &Monitor_lock);
			pthread_cond_wait(&waiting, &Monitor_lock);
		}
		
        numCars++; // increase the number of cars on the bridge
        currentDirection = td.direction; // set the direction the bridge will allow flow through
		
        pthread_mutex_unlock(&Monitor_lock); // let another thread use the monitor	
		wait(WAIT_TIME);
		
		sched_yield();
		
    }
	

    void CrossBridge (thread_data td) {
		pthread_mutex_lock(&Monitor_lock); // block other threads from using the monitor
		
        cout << "Car " << td.id <<" crossing bridge. Current dir: "
        << currentDirection << " #cars: "<< numCars << endl;
		
		pthread_mutex_unlock(&Monitor_lock); // let another thread use the monitor
		wait(WAIT_TIME);
		sched_yield();
		
    }

    void ExitBridge(thread_data td) {
		
    	pthread_mutex_lock(&Monitor_lock); // block other threads from using the monitor
		
    	cout << "Car " << td.id << " dir " << td.direction << " exits from bridge." << endl;

    	numCars--; // car has left the bridge, so decrease number of cars occupying the bridge
		
    	/* if (numCars > 0) { // check if cars are on bridge
		
    	    pthread_cond_broadcast(&NorthSouth[td]);
			
    	}
    	else {
			
    	    if(Waiting[1-td] != 0){ //check if other direction has cars waiting
			
    	        pthread_cond_signal(&NorthSouth[1-td]);
				
    	    }
    	    else{
				
    	        pthread_cond_signal(&NorthSouth[td]);
				
    	    }
			
    	} */
		
		pthread_cond_broadcast(&waiting); // see if there are any threads in the waiting queue that can cross the bridge		
    	pthread_mutex_unlock(&Monitor_lock); // let another thread use the monitor
		sched_yield();

    }

    bool isSafe(int direction) {
		
        if ( numCars == 0 )
			
            return true;    // always safe when bridge is empty
		
        else if ((numCars < MAX_CAPACITY) && (currentDirection == direction))
			
            return true;    // room for us to follow others in td
		
        else
			
            return false;   // bridge is full or has oncoming traffic.
		
    }

};
int Bridge::numCars = 0;
int Bridge::currentDirection = 0;
const int Bridge::MAX_CAPACITY = 3;
Bridge *Bridge::instance = new Bridge();

void Init()
{
     Waiting[0] = Waiting[1] = 0;       /* no vehicle waiting       */
}

void* OneVehicle(void* tdata) {
    /* int direction; //Set inital value to NULL by default
    void *voidPtr = &direction;
    int *directionPtr = static_cast<int*>(voidPtr);
	unsigned int state = (unsigned int)time(0);
	
	// srand(time(0)); // generate new seed for rand() to use
	direction = rand_r(&state) % 2; // generate random number either 0 or 1
	srand(state);

	cout << direction << endl; */


  /*   Bridge::ArriveBridge(direction);
    Bridge::CrossBridge(direction);
    Bridge::ExitBridge(direction); */
	
	// int direction = *(int*) td; // convert void* to int* and dereference
	static Bridge b = *Bridge::GetInstance();
	thread_data td = *(thread_data*) tdata; // convert to structure containing thread data
	
	b.ArriveBridge( td ); 
	b.CrossBridge( td );
	b.ExitBridge( td );
	
	/* Bridge::ArriveBridge( direction );
	Bridge::CrossBridge( direction );
	Bridge::ExitBridge( direction ); */

}

pthread_attr_t attr;
const int NUM_CARS = 50;
int main(int argc, char* argv[]) {
    Init();
    void *voidPtr;
	
	int i = 0;
	// for (i = 0; i < 1000000; i++) cout << i << endl;

    pthread_t cars[NUM_CARS];
	int *direction = new int[NUM_CARS]; // allocate enough space for every car
	thread_data *args = new thread_data[NUM_CARS];
	
	srand(time(NULL)); // set the seed (once) for random number to use on application start
	for (i = 0; i < NUM_CARS; i++) {
		
		args[i].direction = rand() % 2;
		args[i].id = i + 1;
		
		//direction[i] = rand() % 2; // generate a random number either 0 or 1
		
		//cout << direction[i] << endl;
	}

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	
	pthread_mutex_init(&Monitor_lock, NULL);
	pthread_cond_init(&NorthSouth[0], NULL);
	pthread_cond_init(&NorthSouth[1], NULL);
	pthread_cond_init(&waiting, NULL);


	// start multithreading
	for (i = 0; i < NUM_CARS; i++) {
		pthread_create(&cars[i], &attr, OneVehicle, &args[i]); // create thread for car using random direction
	}
		  
	for (i = 1; i <= NUM_CARS; i++) {
		pthread_join(cars[i], NULL);
		//cout << "thread " << args[i].id << " joined" << endl;
	}
	//cout << "All threads joined!" << endl;
		  
	pthread_mutex_destroy(&Monitor_lock);
	pthread_cond_destroy(&NorthSouth[0]);
	pthread_cond_destroy(&NorthSouth[1]);
	
	pthread_exit(0);
}
