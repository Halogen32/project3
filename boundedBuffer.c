/*
 * boundedBuffer.c
 *
 * A complete example of simple producer/consumer program. The Producer
 * and Consumer functions are executed as independent threads.  They
 * share access to a single buffer, data.  The producer deposits a sequence
 * of integers from 1 to numIters into the buffer.  The Consumer fetches
 * these values and adds them.  Two semaphores,empty and full are used to
 * ensure that the producer and consumer alternate access to the buffer.
 *
 * SOURCE: adapted from example code in "Multithreaded, Parallel, and
 *         Distributed Programming" by Gregory R. Andrews.
 */
 
 // modifiers: Mitchell Myers, Maria Moore
 // group: 32
 // date: 3/10/2019
 // modified: 3/14/2019
 // purpose: modify the program according to the the specifications in P3
 // project: P3, part 1
 
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>

#define SHARED 1 // semaphore variables are shared between processes

// method forward declarations (aka prototypes)
void *Producer (void *); // method for producer thread
void *Consumer (void *); // method for consumer thread

sem_t empty; // empty buffer semaphore
sem_t full; // full buffer semaphore
sem_t mutex; // mutual exclusion vars

int numIters; // number of cycles producers/consumers loop through to assign data
int bufferSize; // how big the dynamic array should be
int* dataBuffer; // dynamic array containing data to be consumed

// describes what the index is for in the argv[]
enum ArgvIndex {
	ITERATIONS = 1,
	SIZE = 2
};

// main() -- read command line and create threads
int main(int argc, char *argv[]) { // NOTE: argc = argument counter; argv = argument variable (array)
    pthread_t pid, cid;
    const int EXPECTED_ARGC = 3; // minimum number of arguments expected to pass through cmd line, including the executable
    const int NUM_EACH_THREAD = 3; // number of threads to create of each type (producer/consumer)
 
    if (argc < EXPECTED_ARGC) { // display an error message if there are not enough arguments supplied
	    printf("Usage: boundedBuffer <Number of Iterations> <buffer size>\n");
	    exit(0);
    }

    numIters = atoi(argv[ITERATIONS]); // convert interation string to integer value
	bufferSize = atoi(argv[SIZE]); // convert buffer size string to integer value
	
    sem_init(&empty, SHARED, bufferSize);    // sem empty = n
    sem_init(&full, SHARED, 0); // sem full = 0
    sem_init(&mutex, SHARED, 1);

	dataBuffer = (int*)calloc(bufferSize, sizeof(int)); // dynamic allocate memory for integer array and get its pointer

	// NOTE: modify the program to allow for multiple producers and consumers
	// NOTE: in the main thread, create 3 producers and 3 consumers
    
    for (int i = 0; i < NUM_EACH_THREAD; i++) { // create 'n' threads of each type
		pthread_create(&pid, NULL, Producer, NULL);
		pthread_create(&cid, NULL, Consumer, NULL);
    }

    pthread_join(pid, NULL); // wait until the last created producer reaches this point
    pthread_join(cid, NULL); // wait until the last created consumer reaches this point
    pthread_exit(0); // destroy all threads
}

// deposit 1, ..., numIters into the data buffer
void *Producer(void *arg) {
	static int range = 0;
    int produced;

	
	for (produced = 0; produced < numIters; produced++) {		

		sem_wait(&empty); // wait until an empty buffer slot is available
		sem_wait(&mutex); // wait until the current thread manipulating the buffer is finished
	
		dataBuffer[range] = produced;
		range = (range + 1) % bufferSize; // advance to the next buffer slot; wrap past the end
		
		sem_post(&mutex); // let another thread use the buffer
		sem_post(&full); // let another thread consume from the buffer
		
	}
	
	
}

//fetch numIters items from the buffer and sum them
void *Consumer(void *arg) {
    static int range = 0;
    int total = 0;
    int consumed;

	
    for (consumed = 0; consumed < numIters; consumed++) {
		
		sem_wait(&full); // wait until the buffer has a filled slot
		sem_wait(&mutex); // wait until the current thread is done using the buffer
		
        total = total + dataBuffer[range];
		range = (range + 1) % bufferSize; // advance to the next buffer slot; wrap past the end
		
		sem_post(&mutex); // let another thread use the buffer
		sem_post(&empty); // let another thread produce to the buffer
		
    }
      
    printf("the total is %d\n", total); // print the thread id and a message displaying the total
}
